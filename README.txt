INTRODUCTION
------------

Cookie Banner Implementor module is a simple and intuitive module 
that provides a configuration UI to input JavaScript code. 
The module attaches the script to each page of your website.

REQUIREMENTS
------------

The Cookie Banner Implementor module requires you to have a 
cookie banner script ready. The cookie banner script, which 
must be in JavaScript has to be provided by your cookie provider

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extend/installing-modules
   for further information.

CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions

   - Access Cookie Banner Implementor

     Users in roles with Add Scripts all over the site permission will
     add / remove the  scripts from any position.

 * Add cookie banner script in the required section under ->
   Configuration -> Development -> Cookie Banner Implementor
