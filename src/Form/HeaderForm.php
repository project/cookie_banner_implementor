<?php

namespace Drupal\cookie_implementor\Form;

use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provide settings page for adding cookie_implementor header scripts.
 */
class HeaderForm extends ConfigFormBase {

  /**
   * Implements FormBuilder::getFormId.
   */
  public function getFormId() {
    return 'hfs_header_settings';
  }

  /**
   * Implements ConfigFormBase::getEditableConfigNames.
   */
  protected function getEditableConfigNames() {
    return ['cookie_implementor.header.settings'];
  }

  /**
   * Implements FormBuilder::buildForm.
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $header_section = $this->config('cookie_implementor.header.settings')->get();

    $form['hfs_header']['description'] = [
      '#type'          => 'fieldset',
      '#title'         => $this->t('Getting Started'),
      '#description'   => $this->t('<p>The Cookie Banner Implementor module requires a JavaScript cookie banner script from a legible cookie consent provider. </br><br/>

                          <h4>Implementing the Cookie Banner</h4>

                          <p>1. Copy the whole cookie script code including the script tags</p>
                          <p>2. Place the whole script in the section below</p>
                          <p>3. Save the configuration to publish the cookie banner</p></br>

                          <b>Important: </b>The script must be in JavaScript<br/>


                                     '),
      '#open' => TRUE,
    ];

    $form['hfs_header']['scripts'] = [
      '#type'          => 'textarea',
      '#title'         => $this->t('Insert the Main Script from your cookie provider here after removing the comments, if any.'),
      '#default_value' => isset($header_section['scripts']) ? $header_section['scripts'] : '',
      '#rows'          => 10,
    ];

    $form['hfs_header']['footer'] = [
      '#type'          => 'fieldset',
      '#title'         => $this->t('Disclaimer'),

      '#description'   => $this->t('<p>This module allows you to publish cookie banner and preference center on your Drupal website. Use of this module does not, by itself, ensure compliance with legal requirements related to cookies.</p>

                                     '),
      '#open' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Implements FormBuilder::submitForm().
   *
   * Serialize the user's settings and save it to the Drupal's config Table.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configFactory()
      ->getEditable('cookie_implementor.header.settings')
      ->set('scripts', $values['scripts'])
      ->save();

    $messenger = \Drupal::messenger();
    $messenger->addMessage('Your Settings have been saved.', 'status');
  }

}
